# Skinny Widgets Tree for Default Theme


tree element

```
npm i sk-tree sk-tree-default --save
```

then add the following to your html

```html
<sk-config
    theme="default"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-default"
></sk-config>
<sk-tree link-tpl-str='<a href="?categoryId={{ id }}">{{ name }}</a>' tree-data='[{"id": 1, "name": "category1", "parentId": 0}, {"id": 2, "name": "category2", "parentId": 0}, {"id": 3, "name": "category11", "parentId": 1}, {"id": 4, "name": "category111", "parentId": 3}]'></sk-tree>
<script type="module">
    import { SkTree } from './node_modules/sk-tree/index.js';

    customElements.define('sk-tree', SkTree);
</script>
```

#### attributes

**expanded** - "false" value collapses the tree and renders it as collapsible, "true" renders as expanded but collapsible 

**selected-id** - id of the element that's branch will be forced to be extended

**link-tpl-str** - inline template for each item contents

**item-tpl-path** - external template path for item

**root-tn** - root tag name (default: 'ul')

**item-tn** - item tag name (default: 'li')

**root-cn** - class name for root (default: 'sk-tree-root')

**item-cn** - class name for item (default: 'sk-tree-item')

**branch-cn** - class name for branch (default: 'sk-tree-has-child')


#### template

id: SkTreeTpl, SkTreeItemTpl