
import { SkTreeImpl }  from '../../sk-tree/src/impl/sk-tree-impl.js';

export class DefaultSkTree extends SkTreeImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'tree';
    }

}
